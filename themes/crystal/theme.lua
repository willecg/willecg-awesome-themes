local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gears = require("gears")
shape = gears.shape

local awful = require("awful")
awful.util = require("awful.util")
local wibox = require("wibox")

local lain = require("lain")
local vicious = require("vicious")

local markup = lain.util.markup

local conf = require("conf")

theme_path = awful.util.getdir("config") .. "themes/crystal/"
icon_theme_path = theme_path .. "simple/light/"
theme = {}

if conf.wallpaper == "" then
   theme.wallpaper = theme_path .. "background.jpg"
else
   theme.wallpaper = conf.wallpaper
end

if conf.icon == "" then
   theme.awesome_icon = icon_theme_path .. "awesome_icon.png"
   theme.awesome_subicon = icon_theme_path .. "awesome_icon.png"
else
   theme.awesome_icon = conf.icon
   theme.awesome_subicon = conf.icon
end



theme.font          = "Sans 8"

theme.windows_shape = function(cr, w, h)
   gears.shape.rounded_rect(cr, w, h, 5)
end


-- {{ Color theme
theme.black = "#000000"
theme.red = "#a51d2d"
theme.blue = "#1a5fb4"
theme.green = "#26a269"
theme.white = "#deddda"
theme.orange = "#ff5d00"
theme.gray = "#9a9996"
-- }}

-- {{ Elements
theme.bg_normal     = theme.black .. "50"
theme.bg_focus      = theme.black .. "50"
theme.bg_urgent     = theme.red
theme.bg_minimize   = theme.bg_normal
theme.bg_systray    = theme.blue .. "20"

theme.fg_normal     = theme.white
theme.fg_focus      = theme.white
theme.fg_urgent     = theme.white
theme.fg_minimize   = theme.gray

theme.useless_gap   = dpi(4)
theme.border_width  = dpi(1)
theme.border_normal = theme.white
theme.border_focus  = theme.blue
theme.border_marked = theme.green
theme.wibar_height = dpi(24)
theme.wibar_bg = theme.black .. "50"

theme.taglist_bg_empty = theme.bg_normal .. "00"
theme.taglist_bg_occupied = theme.bg_normal .. "00"
theme.tasklist_bg_focus = theme.bg_focus
theme.tasklist_fg_focus = theme.fg_focus
theme.tasklist_bg_normal = theme.bg_normal .. "00"
theme.tasklist_fg_normal = theme.fg_normal

theme.hotkeys_bg = theme.white 
theme.hotkeys_modifiers_fg = theme.black
theme.hotkeys_label_fg = theme.black
theme.hotkeys_fg = theme.black

theme.taglist_squares_sel = theme_path .. "squares_sel.svg"

theme.taglist_squares_unsel = theme_path .. "squares_unsel.svg"


theme.notification_font = "Cantarell 9"
theme.notification_bg = theme.bg_normal
theme.notification_fg = theme.white
theme.notification_border_width = 1
theme.notification_border_color = theme.blue
theme.notification_margin = 20
theme.notification_shape = theme.windows_shape


-- Variables set for theming the menu:
theme.menu_bg_normal = theme.bg_normal
theme.menu_fg_normal = theme.fg_normal
theme.menu_bg_focus = theme.blue
theme.menu_fg_focus = theme.white
theme.menu_border_color = theme.blue


theme.menu_submenu_icon = icon_theme_path.."submenu.svg"
theme.menu_height = dpi(20)
theme.menu_width  = dpi(150)

theme.bat_empty_con = icon_theme_path .. "widgets/bat_empty_con.svg"
theme.bat_empty = icon_theme_path .. "widgets/bat_empty.svg"
theme.bat_full_con = icon_theme_path .. "widgets/bat_full_con.svg"
theme.bat_full = icon_theme_path .. "widgets/bat_full.svg"
theme.bat_low_con = icon_theme_path .. "widgets/bat_low_con.svg"
theme.bat_low = icon_theme_path .. "widgets/bat_low.svg"
theme.bat_mid_con = icon_theme_path .. "widgets/bat_mid_con.svg"
theme.bat_mid = icon_theme_path .. "widgets/bat_mid.svg"
theme.cpu = icon_theme_path .. "widgets/cpu.svg"
theme.filesystem = icon_theme_path .. "widgets/filesystem.svg"
theme.mail = icon_theme_path .. "widgets/mail.svg"
theme.mail_new = icon_theme_path .. "widgets/mail-new.svg"
theme.ram = icon_theme_path .. "widgets/ram.svg"
theme.temp = icon_theme_path .. "widgets/temp.svg"
theme.todo = icon_theme_path .. "widgets/todo.svg"
theme.up_down = icon_theme_path .. "widgets/up_down.svg"
theme.vol_high = icon_theme_path .. "widgets/volume-high.svg"
theme.vol_low = icon_theme_path .. "widgets/volume-low.svg"
theme.vol_mid1 = icon_theme_path .. "widgets/volume-mid1.svg"
theme.vol_mid2 = icon_theme_path .. "widgets/volume-mid2.svg"
theme.vol_mute = icon_theme_path .. "widgets/volume-mute.svg"
theme.net = icon_theme_path .. "widgets/net.svg"
theme.hd = icon_theme_path .. "widgets/hd.svg"

theme.wifi_weak = icon_theme_path .. "widgets/wifi-weak.svg"
theme.wifi_mid = icon_theme_path .. "widgets/wifi-mid.svg"
theme.wifi_good = icon_theme_path .. "widgets/wifi-good.svg"
theme.wifi_great = icon_theme_path .. "widgets/wifi-great.svg"
-- }} Elements

-- {{ Title bar elements
theme.titlebar_close_button_normal = icon_theme_path.."titlebar/close_normal.svg"
theme.titlebar_close_button_focus  = icon_theme_path.."titlebar/close_focus.svg"

theme.titlebar_minimize_button_normal = icon_theme_path.."titlebar/minimize_normal.svg"
theme.titlebar_minimize_button_focus  = icon_theme_path.."titlebar/minimize_focus.svg"

theme.titlebar_ontop_button_normal_inactive = icon_theme_path.."titlebar/ontop_normal.svg"
theme.titlebar_ontop_button_focus_inactive  = icon_theme_path.."titlebar/ontop_focus.svg"
theme.titlebar_ontop_button_normal_active = icon_theme_path.."titlebar/ontop_active_normal.svg"
theme.titlebar_ontop_button_focus_active  = icon_theme_path.."titlebar/ontop_active_focus.svg"

theme.titlebar_sticky_button_normal_inactive = icon_theme_path.."titlebar/sticky_normal.svg"
theme.titlebar_sticky_button_focus_inactive  = icon_theme_path.."titlebar/sticky_focus.svg"
theme.titlebar_sticky_button_normal_active = icon_theme_path.."titlebar/sticky_active_normal.svg"
theme.titlebar_sticky_button_focus_active  = icon_theme_path.."titlebar/sticky_active_focus.svg"

theme.titlebar_floating_button_normal_inactive = icon_theme_path.."titlebar/floating_normal.svg"
theme.titlebar_floating_button_focus_inactive  = icon_theme_path.."titlebar/floating_focus.svg"
theme.titlebar_floating_button_normal_active = icon_theme_path.."titlebar/floating_active_normal.svg"
theme.titlebar_floating_button_focus_active  = icon_theme_path.."titlebar/floating_active_focus.svg"

theme.titlebar_maximized_button_normal_inactive = icon_theme_path.."titlebar/maximize_normal.svg"
theme.titlebar_maximized_button_focus_inactive  = icon_theme_path.."titlebar/maximize_focus.svg"
theme.titlebar_maximized_button_normal_active = icon_theme_path.."titlebar/maximize_active_normal.svg"
theme.titlebar_maximized_button_focus_active  = icon_theme_path.."titlebar/maximize_active_focus.svg"
-- }} Title bar elements

--{{
theme.layout_fairh = icon_theme_path.."layouts/fairh.svg"
theme.layout_fairv = icon_theme_path.."layouts/fairv.svg"
theme.layout_floating  = icon_theme_path.."layouts/floating.svg"
theme.layout_magnifier = icon_theme_path.."layouts/magnifier.svg"
theme.layout_max = icon_theme_path.."layouts/max.svg"
theme.layout_fullscreen = icon_theme_path.."layouts/fullscreen.svg"
theme.layout_tilebottom = icon_theme_path.."layouts/tilebottom.svg"
theme.layout_tileleft   = icon_theme_path.."layouts/tileleft.svg"
theme.layout_tile = icon_theme_path.."layouts/tileright.svg"
theme.layout_tiletop = icon_theme_path.."layouts/tiletop.svg"
theme.layout_spiral  = icon_theme_path.."layouts/spiral.svg"
theme.layout_dwindle = icon_theme_path.."layouts/dwindle.svg"
theme.layout_cornernw = icon_theme_path.."layouts/cornernw.svg"
theme.layout_cornerne = icon_theme_path.."layouts/cornerne.svg"
theme.layout_cornersw = icon_theme_path.."layouts/cornersw.svg"
theme.layout_cornerse = icon_theme_path.."layouts/cornerse.svg"
theme.layout_termfair = icon_theme_path.."layouts/termfair.svg"
theme.layout_centerfair = icon_theme_path.."layouts/centerfair.svg"
theme.layout_cascade = icon_theme_path.."layouts/cascade.svg"
theme.layout_cascadetile = icon_theme_path.."layouts/cascadetile.svg"
theme.layout_centerwork = icon_theme_path.."layouts/centerwork.svg"
theme.layout_centerworkh = icon_theme_path.."layouts/centerworkh.svg"

--}}


-- {{ Widgets
local mylauncher = awful.widget.button({image = theme.awesome_icon})
mylauncher:connect_signal("button::press", function() awful.util.mymainmenu:toggle() end)

mytextclock = wibox.widget.textclock("%A, %d de %B del %Y. %H:%M:%S", 1)
tc_background = wibox.widget {

   {
      mytextclock,
      bg = theme.bg_normal,
      widget = wibox.container.background,
   },

   layout = wibox.layout.fixed.horizontal
}

-- Calendario
local calendario = lain.widget.cal({
      attach_to = {mytextclock},
      notification_preset = {
         font = "Monospace 10",
         fg = theme.notification_fg,
         bg = theme.notification_bg,
         position = "top_right"
      }
})

st_background = wibox.widget {
   {
      wibox.widget.systray(),
      --margin = 4,
      widget = wibox.container.background
   },
   layout = wibox.layout.fixed.horizontal
}

-- mpd
theme.mpd = lain.widget.mpd({
      
      settings = function ()
         if mpd_now.state == "stop" then
            widget:set_markup("")
         elseif mpd_now.state == "pause" then
            widget:set_markup(" 🎜  ⏸   " .. mpd_now.artist .. " - " .. mpd_now.title .. "   🎝      ")
         else
            widget:set_markup("<span foreground=\"".. theme.fg_focus .."\"> 🎜</span> <span foreground=\"".. theme.border_marked .."\"> ▶ </span> " .. mpd_now.artist .. " - " .. mpd_now.title .. "<span foreground=\"".. theme.fg_focus .."\"> 🎜</span>")
         end
      end
})

-- weather widget
local i_weather = wibox.widget.imagebox(theme.weather)
wth = lain.widget.weather({
      city_id = conf.weather_city,
      APPID = conf.weather_appid,
      lang = conf.lang,
      cnt = conf.weather_cnt,
      lat = conf.weather_lat,
      lon = conf.weather_lon,
      notification_preset = {
         font = "Terminus 10",
         fg = theme.notification_fg,
         bg = theme.notification_bg,
         position = "top_right"
      },
      settings = function ()
         units = math.floor(weather_now["main"]["temp"])
         widget:set_markup(" " .. units .. "°C ")
      end
})

wea_background = wibox.widget {
   wth.icon,
   wth.widget,
   layout = wibox.layout.fixed.horizontal,
   forced_height = 20
}

-- imap widget
local data = ""
local i_mail = wibox.widget.imagebox(theme.mail)
local mail = lain.widget.imap({
      server = conf.mail_server,
      mail = conf.mail_account,
      timeout = 60,
      password = function ()
         awful.spawn.easy_async(
            conf.mail_command,
            function(stdout, stderr, reason, exit_code)
               data = stdout
            end
         )
         
         if data == "" then
            return data, true
         end
         local passwd = string.sub(data, 1, -2)
         data = ""
         return passwd, false
      end,
      
      settings = function ()
         if mailcount > 0 then
            i_mail:set_image(theme.mail_new)
            widget:set_markup(mailcount)
         else
            
            if imap_now["MESSAGES"] == 0 then
               widget:set_markup("Cargando...")
               i_mail:set_image(theme.mail)
            else
               widget:set_markup("")
               i_mail:set_image(theme.mail)
            end
            
            i_mail:set_image(theme.mail)
         end
      end,

      mail_notification_preset = {
         position = "top_right",
         icon = theme.mail_new
      }
})

mail_background = wibox.widget {
   i_mail,
   mail.widget,
   layout = wibox.layout.fixed.horizontal,
   forced_height = 20
}


local i_org = wibox.widget.imagebox(theme.todo)
local org = wibox.widget.textbox()

agendafiles = conf.agendafiles
vicious.register(
   org,
   vicious.widgets.org,
   "$1|$2|$3|$4", 199, agendafiles
)

local org_background = wibox.widget {
   i_org,
   org,

   layout = wibox.layout.fixed.horizontal
}

-- cpu widget
local i_cpu = wibox.widget.imagebox(theme.cpu)
local progress_cpu = wibox.widget{
   max_value = 100,
   value = 0,
   forced_width = 50,
   paddings = 1,
   border_width = 1,
   border_color = theme.fg_normal,
   background_color = theme.bg_normal .. "00",
   color = theme.blue,
   bar_shape = gears.shape.rounded_rect,
   shape = gears.shape.rounded_rect,
   widget = wibox.widget.progressbar
}
local cpu = lain.widget.cpu {
   settings = function()
      i_cpu:set_image(theme.cpu)
      if cpu_now.usage >=80 then
         widget:set_markup('<span foreground="' .. theme.bg_urgent .. '"> ' ..cpu_now.usage .. '%</span> ')
      else
         widget:set_markup(' ' .. cpu_now.usage .. '% ')
      end
      progress_cpu.value = cpu_now.usage
   end
}
cpu_background = wibox.widget {
   i_cpu,
   wibox.widget{
      progress_cpu,
      cpu.widget,
      layout = wibox.layout.stack
   },
   layout = wibox.layout.fixed.horizontal,
   forced_height = 24
}

-- mem widget
local i_ram = wibox.widget.imagebox(theme.ram)
local progress_ram = wibox.widget{
   max_value = 100,
   value = 0,
   forced_width = 50,
   paddings = 1,
   border_width = 1,
   border_color = theme.fg_normal,
   background_color = theme.bg_normal .. "00",
   color = theme.blue,
   bar_shape = gears.shape.rounded_rect,
   shape = gears.shape.rounded_rect,
   widget = wibox.widget.progressbar
}
local ram = lain.widget.mem {
   settings = function()
      i_ram:set_image(theme.ram)
      progress_ram.value = mem_now.perc
      if mem_now.perc >= 80 then
         widget:set_markup('<span foreground="  ' .. theme.bg_urgent .. '"> ' .. mem_now.perc .. '%</span>')
      else
         widget:set_markup('  ' .. mem_now.perc .. '%')
      end
   end
}
ram_background = wibox.widget {
   i_ram,
   wibox.widget{
      progress_ram,
      ram.widget,
      layout = wibox.layout.stack
   },
   layout = wibox.layout.fixed.horizontal,
   forced_height = 24
}

-- net widget
local wifi_icon = wibox.widget.imagebox()

local eth_icon = wibox.widget.imagebox()
local net = lain.widget.net {
   iface = conf.wlface,
   wifi_state = "on",
   eth_state = "on",
   settings = function()
      local eth0 = net_now.devices[conf.ethface]
      if eth0 then
         if eth0.ethernet then
            eth_icon:set_image(theme.net)
         else
            eth_icon:set_image()
         end
      end

      local wlan0 = net_now.devices[conf.wlface]
      if wlan0 then
         if wlan0.wifi then
            local signal = wlan0.signal
            if signal < -83 then
               wifi_icon:set_image(theme.wifi_weak)
            elseif signal < -70 then
               wifi_icon:set_image(theme.wifi_mid)
            elseif signal < -53 then
               wifi_icon:set_image(theme.wifi_good)
            elseif signal >= -53 then
               wifi_icon:set_image(theme.wifi_great)
            end
         else
            wifi_icon:set_image()
         end
      end
   end
}
net_background = wibox.widget {
   eth_icon,
   wifi_icon,
   net.widget,
   layout = wibox.layout.fixed.horizontal,
   forced_height = 24
}

-- vol widget
local i_vol = wibox.widget.imagebox(theme.vol_mute)
local vol = awful.widget.watch(
   'bash -c "pactl get-sink-volume @DEFAULT_SINK@ | cut -s -d/ -f2; pactl get-sink-mute @DEFAULT_SINK@"',
   2, function(widget, stdout)
      local volume = 0
      local channels = 0
      local voloutput = ""
      for v in stdout:gmatch("%d+") do
         volume = volume + v
         channels = channels + 1
      end
      if channels == 0 then
         voloutput = "N/A"
      else
         volume = math.floor(volume / channels)
         voloutput = volume .. "%"
      end
      
      local mute = string.match(stdout, "Mute: (%S+)") or "N/A"
      
      if mute == "N/A" or mute == "sí" then
         i_vol:set_image(theme.vol_mute)
      elseif volume <= 10 then
         i_vol:set_image(theme.vol_low)
      elseif volume <= 25 then
         i_vol:set_image(theme.vol_mid1)
      elseif volume <= 85 then
         i_vol:set_image(theme.vol_mid2)
      else
         i_vol:set_image(theme.vol_high)         
      end
      widget:set_markup(voloutput)
      
   end
)

theme.vol_background = wibox.widget {
   i_vol,
   vol,
   layout = wibox.layout.fixed.horizontal,
   forced_height = 24
}


i_bat = wibox.widget.imagebox(theme.bat_low)
bat = lain.widget.bat({
      timeout = 10,
      battery="BAT1",
      settings = function()
         widget:set_markup(tonumber(bat_now.perc))
         if bat_now.status == "Charging" then
            widget:set_markup(tonumber(bat_now.perc) .. "%")
            if tonumber(bat_now.perc) <= 15 then
               i_bat:set_image(theme.bat_empty_con)
            elseif tonumber(bat_now.perc) <= 50 then
               i_bat:set_image(theme.bat_low_con)
            elseif tonumber(bat_now.perc) <= 85 then
               i_bat:set_image(theme.bat_mid_con)
            else
               i_bat:set_image(theme.bat_full_con)
            end
         elseif bat_now.status == "Discharging" then
            widget:set_markup(tonumber(bat_now.perc) .. "%")
            if tonumber(bat_now.perc) <= 15 then
               i_bat:set_image(theme.bat_empty)
            elseif tonumber(bat_now.perc) <= 50 then
               i_bat:set_image(theme.bat_low)
            elseif tonumber(bat_now.perc) <= 85 then
               i_bat:set_image(theme.bat_mid)
            else
               i_bat:set_image(theme.bat_full)
            end
         elseif bat_now.status == "Full" then
            widget:set_markup(" Cargado ")
            i_bat:set_image(theme.bat_full)
         end
      end
})

bat_background = wibox.widget {
   i_bat,
   bat.widget,
   layout = wibox.layout.fixed.horizontal,
   forced_height = 24
}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()
lk_background = wibox.widget{
   mykeyboardlayout,
   layout = wibox.layout.fixed.horizontal,
   forced_height = 20 
}

-- }} Widgets



function theme.screen_connect(s)
   
   local wallpaper = theme.wallpaper
   if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
   end
   gears.wallpaper.maximized(wallpaper, s, true)

   s.mypromptbox = awful.widget.prompt()
   -- Create a taglist
   awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
   -- We need one layoutbox per screen.
   s.mylayoutbox = awful.widget.layoutbox(s)
   s.mylayoutbox:buttons(gears.table.join(
                            awful.button({ }, 1, function () awful.layout.inc( 1) end),
                            awful.button({ }, 3, function () awful.layout.inc(-1) end),
                            awful.button({ }, 4, function () awful.layout.inc( 1) end),
                            awful.button({ }, 5, function () awful.layout.inc(-1) end)))
   
   -- Create a taglist widget
   s.mytaglist = awful.widget.taglist {
      screen  = s,
      filter  = awful.widget.taglist.filter.all,
      buttons = awful.util.taglist_buttons,
      style  = {
         shape = gears.shape.rect,
         spacing = 2,
      }
   }

   s.mytasklist = awful.widget.tasklist {
      screen  = s,
      filter  = awful.widget.tasklist.filter.currenttags,
      buttons = awful.util.tasklist_buttons,
      style = {
         shape_border_width = 1,
         shape_border_color = theme.border_normal,
         shape  = gears.shape.rounded_rect,
      },
      layout = {
         spacing = 10,
         spacing_widget = {
            {
               forced_width = 2,
               shape        = gears.shape.circle,
               widget       = wibox.widget.separator
            },
            valign = 'center',
            halign = 'center',
            widget = wibox.container.place,
         },
         layout  = wibox.layout.flex.horizontal
      },
      -- Notice that there is *NO* wibox.wibox prefix, it is a template,
      -- not a widget instance.
      widget_template = {
         {
            {
               {
                  {
                     id     = 'icon_role',
                     widget = wibox.widget.imagebox,
                  },
                  margins = 2,
                  widget  = wibox.container.margin,
               },
               {
                  id     = 'text_role',
                  widget = wibox.widget.textbox,
               },
               layout = wibox.layout.fixed.horizontal,
            },
            left  = 10,
            right = 10,
            widget = wibox.container.margin
         },
         id     = 'background_role',
         widget = wibox.container.background,
      },

   }
   ltsk_background = wibox.widget {
      {
         s.mytasklist,
         bg = theme.bg_normal,
         widget = wibox.container.background
      },
      layout = wibox.layout.align.horizontal
   }
   

   -- Create the wibox
   local wibox_shape = function(cr, width, height)
      gears.shape.partially_rounded_rect(cr, width, height, true, true, true, true, dpi(8))
   end

   s_width = s.workarea.width - (theme.useless_gap + theme.border_width) * 4
   s.mwibox = awful.wibar({ position = "top", screen = s, height = theme.wibar_height,
                            width = s_width, shape = wibox_shape})

   -- Add widgets to the wibox
   local h_margin = 4
   local v_margin = 4
   -- -- Add widgets to the wibox
   s.mwibox:setup {
      layout = wibox.layout.align.horizontal,
      { -- Left widgets
         layout = wibox.layout.fixed.horizontal,
         wibox.container.margin(mylauncher, h_margin + 4, h_margin, v_margin, v_margin),

         s.mytaglist,
         s.mypromptbox,
      },
      s.mytasklist, -- Middle widget
      { -- Right widgets
         layout = wibox.layout.fixed.horizontal,
         spacing = 4,
         theme.mpd,
         wibox.container.margin(wea_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(mail_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(org_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(cpu_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(ram_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(net_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(theme.vol_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(bat_background, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(wibox.widget.systray(), h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(mytextclock, h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(s.mylayoutbox, h_margin, h_margin + 4, v_margin, v_margin),
      },
   }
end

function theme.set_title_bar(c)
   local buttons = gears.table.join(
      awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
      end),
      awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
      end)
   )
   awful.titlebar(c,
                  {size = 16}) : setup {
      { -- Left
         wibox.container.margin(awful.titlebar.widget.iconwidget(c), h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(awful.titlebar.widget.floatingbutton (c), h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(awful.titlebar.widget.ontopbutton    (c), h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(awful.titlebar.widget.stickybutton   (c), h_margin, h_margin, v_margin, v_margin),
         layout  = wibox.layout.fixed.horizontal
         
      },
      { -- Middle
         { -- Title
            align  = "center",
            widget = awful.titlebar.widget.titlewidget(c)
         },
         buttons = buttons,
         layout  = wibox.layout.flex.horizontal
      },
      { -- Right
         wibox.container.margin(awful.titlebar.widget.minimizebutton(c), h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(awful.titlebar.widget.maximizedbutton(c), h_margin, h_margin, v_margin, v_margin),
         wibox.container.margin(awful.titlebar.widget.closebutton    (c), h_margin, h_margin, v_margin, v_margin), 
         layout = wibox.layout.fixed.horizontal()
      },
      layout = wibox.layout.align.horizontal
                                       }
   
   
   awful.titlebar.hide(c)
end


return theme
